module Main where

import Test.QuickCheck
import Test.QuickCheck.Checkers
import Test.QuickCheck.Classes
import Trie

import Data.Char
import Data.List (nubBy, sort, sortBy)

instance (CoArbitrary k, CoArbitrary v) => CoArbitrary (Trie k v) where
  coarbitrary = coarbitrary . toList

instance (Eq k, Arbitrary v, Arbitrary k) => Arbitrary (Trie k v) where
  arbitrary = fromList <$> arbitrary

instance (Eq k, Eq v) => EqProp (Trie k v) where
  (=-=) = eq

sanitize :: [(String, String)] -> [(String, String)]
sanitize = sortBy (eq compare)
         . nubBy (eq (==))
         . filter (\(k, v) -> k /= "" && v /= "")
  where eq f (k, _) (k', _) = k `f` k'

main :: IO ()
main = do
  quickCheck ((\s -> s == "" || exists s (insert (s,s) empty)) :: String -> Bool)
  quickCheck ((\s s' -> not (s /= s && exists s (insert (s',s') empty))) :: String -> String -> Bool)
  quickCheck ((\s -> (sanitize . toList . fromList . sanitize) s  == sanitize s) :: [(String, String)] -> Bool)
  quickBatch $ functor (undefined :: [(Trie Char Char, Trie Char Char, Trie Char Char)])
  quickBatch $ monoid  (undefined :: Trie Char Char)
