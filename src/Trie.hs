module Trie
(
    Trie
  , empty
  , insert
  , exists
  , fromList
  , toList
) where

import Control.Monad
import Data.Maybe

data Node k v = Node {
    key :: [k]
  , value :: Maybe v
  , children :: Trie k v
} deriving (Eq, Show)

newtype Trie k v =
  Trie [Node k v]
  deriving (Eq, Show)

instance Functor (Node a) where
  fmap f (Node k v cs) = Node k (fmap f v) (fmap f cs)

instance Functor (Trie a) where
  fmap f (Trie ns) = Trie ((fmap . fmap) f ns)

instance (Eq k) => Monoid (Trie k v) where
  mempty = Trie []
  (Trie []) `mappend` (Trie ys) = Trie ys
  (Trie (x:xs)) `mappend` (Trie ys) =
    Trie xs `mappend` Trie (merge x ys)

instance Foldable (Trie k) where
  foldr _ z (Trie []) = z
  foldr f z (Trie (Node k v cs : ns)) =
    let zch = maybe z (`f` z) v
    in foldr f (foldr f zch cs) (Trie ns)

empty :: Trie k v
empty = Trie []

fromList :: (Eq k) => [([k], v)] -> Trie k v
fromList [] = empty
fromList (a:as) = go [] a empty `mappend` fromList as
  where go :: (Eq k) => [k] -> ([k], v) -> Trie k v -> Trie k v
        go _ ([], _)     f         = f
        go p ([k], v)    t         = Trie [Node (k:p) (Just v) empty]
        go p (x:xs, v)   (Trie []) =
          Trie [Node (x:p) Nothing (go (x:p) (xs, v) empty)]

merge :: (Eq k) => Node k v -> [Node k v] -> [Node k v]
merge n [] = [n]
merge n@(Node k v cs) (Node k' v' cs' : ns)
  | k == k' = Node k (v `mplus` v') (cs `mappend` cs') : ns
  | otherwise = Node k' v' cs' : merge n ns

insert :: (Eq k) => ([k], v) -> Trie k v -> Trie k v
insert a t = fromList [a] `mappend` t

foldWithKey :: (([k], v) -> b -> b) -> b -> Trie k v -> b
foldWithKey _ z (Trie []) = z
foldWithKey f z (Trie (Node k v cs : ns)) =
  let zch = maybe z (`f` z) $ (reverse k, ) <$> v
  in foldWithKey f (foldWithKey f zch cs) (Trie ns)

toList :: Trie k v -> [([k], v)]
toList = foldWithKey (:) []

exists :: (Eq v) => v -> Trie k v -> Bool
exists v = foldr (\v' found -> found || v == v') False

values :: Trie k v -> [v]
values = foldr (:) []

keys :: Trie k v -> [[k]]
keys = foldWithKey (\(k, v) -> (k:)) []
